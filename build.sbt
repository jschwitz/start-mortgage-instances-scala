import Dependencies._

ThisBuild / scalaVersion     := "2.12.10"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "ScalaStart",
    libraryDependencies += scalaTest % Test
  )

// lihaoy/requests-scala
libraryDependencies += "com.lihaoyi" %% "requests" % "0.5.1"

// lihaoy JSON library
// libraryDependencies += "com.lihaoyi" %% "ujson" % "0.7.1"


// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
