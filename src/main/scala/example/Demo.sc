import java.io._
import requests._
//import ujson._


def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
  dir.listFiles.filter(_.isFile).toList.filter { file =>
    extensions.exists(file.getName.endsWith(_))
  }
}

val okFileExtensions = List("pdf","txt")
val files = getListOfFiles(new File("/Users/johnschwitz/mortgage"), okFileExtensions)

files.foreach {println}

val list_mortgages = List("/Users/js/mort01.pdf","/Users/js/mort02.pdf","/Users/js/mort03.pdf")

val req_beg = "{\"variables\": {\"mortgage\" : {\"value\" : \""
val req_end = "\",\"type\": \"String\"}}}"

import scala.collection.mutable.ListBuffer


var list_buf = new ListBuffer[String]()
for (path <- list_mortgages){
  var f1 = new java.io.File(path)
  list_buf += f1.getName()
}
val list_f = list_buf.toList
list_f