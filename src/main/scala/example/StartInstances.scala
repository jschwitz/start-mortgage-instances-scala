package example

import java.io._
import requests._
// Library to manipulate JSON
//import ujson._

object StartInstances extends App {

  // Retrieve path of all files in directory
  // returns a List of File
  def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
    dir.listFiles.filter(_.isFile).toList.filter(file => {
      extensions.exists(ext => {ext.equals(file.getName.split("\\.").last)})
    })
  }

  val okFileExtensions = List("pdf", "txt")
  // I have 10 mortgages in this directory
  val list_mortgages = getListOfFiles(new File("/Users/johnschwitz/mortgage"), okFileExtensions)

  list_mortgages.foreach {
    println
  }

  // val list_mortgages2 = List("/Users/js/mort44.pdf", "/Users/js/mort45.pdf", "/Users/js/mort46.pdf")

  // Construct JSON for Post
  val req_beg = "{\"variables\": {\"mortgage\" : {\"value\" : \""
  val req_end = "\",\"type\": \"String\"}}}"

  // Use mutable list
  import scala.collection.mutable.ListBuffer

  // Do not know why list_mortgages DOES NOT WORK
  // path should be type File; however error is Type String
  // var list_buf = new ListBuffer[String]()
  // for (path <- list_mortgages2) {
  //   var f1 = new java.io.File(path)
  //   list_buf += f1.getName()
  // }
  // val list_f = list_buf.toList

  // list_f.foreach {
  //   println
  // }

  // Initiates Process Instances for a list

  // for (mort <- list_mortgages2) {
  for (mort <- list_mortgages) {
    var json_call = req_beg+mort.getAbsolutePath()+req_end
    requests.post("http://localhost:8080/engine-rest/process-definition/key/validate-instance/start", headers = Map("Content-Type" -> "application/json"), data = json_call)}

}
